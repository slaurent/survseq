#publicly available dependencies
library(stats4)
library(dplyr)
library(BiasedUrn)
library(ggpubr)

#this is the only bioconductor package  - for visualization
if (!requireNamespace("BiocManager", quietly = TRUE))
  install.packages("BiocManager")

BiocManager::install("ggbio")

#this is because hashmap is not in CRAN anymore (only used for simple visualization)
library(devtools)
install_local("hashmap_0.2.2.tar.gz")

#local functions
source("include/misc.R")
source("include/read_genotype_probabilities.R")
source("include/model1.R")
source("include/model2.R")
source("include/selection_inference_1_locus.R")
source("include/model2_wSelection_1_locus.R")

#local functions for visualization
source("include_visualization/visualize_agouti_lrt_pvalues.R")
source("include_visualization/visualize_agouti_proba_central.R")
source("include_visualization/visualize_agouti_delta_p.R")
source("include_visualization/visualize_agouti_delta_p_2.R")
source("include_visualization/visualize_agouti_delta_p_vs_proba_central.R")
source("include_visualization/visualize_null_distribution_model2.R")
source("include_visualization/visualize_null_distributions.R")
source("include_visualization/visualize_all_model2_wSelection.R")


#---------------USER-DEFINED-VARIABLES---------------------------------------------------------------#

#Loading catherine linnen's gemma results for the Agouti locus. (published in Pfeifer, Laurent, Sousa, Linnen, et al 2017)
path_to_annotation_file="annotation/catherine_linnens_gemma_results.csv"

#Minimum size of a genotype class for a given SNP at the beginning of the experiment (i.e. # of individuals with a given genotype)
min_geno_count=0
#Number of replicates for model2 (calculate the distribution of significant sites under model1 assuming random sampling)
nrep_model2=3
#Number of replicates for model2_with_selection (calculate the distribution of significant sites under model1 assuming selection on a single site)
nrep_model2_wSelection=3


#----------------------------------------------------------------------------------------------------#


#READ-IN ANNOTATION------------------------------------------------------------------------------#
#load annotation data (e.g. GEMMA results)
if(!file.exists("session/gemma_results.bin"))                                          # If annotation.bin file does not exist. Read-in from text file.
{
  annotation=read.table(path_to_annotation_file, header=T, sep=";")                  # Open file containing annotation or external results (e.g. Gemma). Needs to contain coordinates in two columns "chr" and "pos"
  save(annotation, file="session/gemma_results.bin")
}
load("session/gemma_results.bin")
#----------------------------------------------------------------------------------------------------#



# -------------READ DATA ---------------------------------------------------------------------------------------------#
#--------------DARK SOIL
#--------------Read-in Agouti data
path_to_data_dark="genetic_data/agouti/dark_soil/"
input_file_dark="agouti_pooled_dark_soil_80s.recode"
lambda_dark=read.table(paste(path_to_data_dark, input_file_dark, "-pheno.txt",sep=""))                  # read mortality vector
map_dark=read.table(paste(path_to_data_dark, input_file_dark, "-coordinate_map.csv", sep=""), header=T) # read in table mapping locus ids and coordinates (both references)
nindiv_dark=length(lambda_dark)                                                                         # number of individuals
nindiv2_dark=sum(lambda_dark)                                                                           # number of survivors 

if(!file.exists(paste("session/",input_file_dark,".input_matrix", sep="")))                             # If matrix exist load from previously saved file if not create, save, and load
{
  geno_matrix_agouti_dark=read_genotype_probabilities(paste(path_to_data_dark, input_file_dark,"-geno.txt",sep=""), nindiv_dark)
  save(geno_matrix_agouti_dark, file =paste("session/", input_file_dark, ".input_matrix",sep=""))
}
load(paste("session/", input_file_dark, ".input_matrix", sep=""))

#--------------Read-in Background_data--
path_to_data_bg_dark="genetic_data/background/dark_soil/"
input_file_bg_dark="background_dark_pooled_80s.recode"
map_bg_dark=read.table(paste(path_to_data_bg_dark, input_file_bg_dark, "-coordinate_map.csv", sep=""), header=T) # read in table mapping locus ids and coordinates (both references)

if(!file.exists(paste("session/",input_file_bg_dark,".input_matrix", sep="")))                                   # If matrix exist load from previously saved file if not create, save, and load
{
  geno_matrix_bg_dark=read_genotype_probabilities(paste(path_to_data_bg_dark, input_file_bg_dark,"-geno.txt",sep=""), nindiv_dark)
  save(geno_matrix_bg_dark, file =paste("session/", input_file_bg_dark, ".input_matrix",sep=""))
}
load(paste("session/", input_file_bg_dark, ".input_matrix", sep=""))


#--------------LIGHT SOIL--
#--------------Read-in Agouti data--
path_to_data_light="genetic_data/agouti/light_soil/"
input_file_light="agouti_pooled_light_soil_80s.recode"
lambda_light=read.table(paste(path_to_data_light, input_file_light, "-pheno.txt",sep=""))                  # read mortality vector
map_light=read.table(paste(path_to_data_light, input_file_light, "-coordinate_map.csv", sep=""), header=T) # read in table mapping locus ids and coordinates (both references)
nindiv_light=length(lambda_light)                                                                          # number of individuals
nindiv2_light=sum(lambda_light)                                                                            # number of survivors 

if(!file.exists(paste("session/",input_file_light,".input_matrix", sep="")))                               # If matrix exist load from previously saved file if not create, save, and load
{
  geno_matrix_agouti_light=read_genotype_probabilities(paste(path_to_data_light, input_file_light,"-geno.txt",sep=""), nindiv_light)
  save(geno_matrix_agouti_light, file =paste("session/", input_file_light, ".input_matrix",sep=""))
}
load(paste("session/", input_file_light, ".input_matrix", sep=""))

#--------------Read-in Background_data--
path_to_data_bg_light="genetic_data/background/light_soil/"
input_file_bg_light="background_light_pooled_80s.recode"
map_bg_light=read.table(paste(path_to_data_bg_light, input_file_bg_light, "-coordinate_map.csv", sep=""), header=T) # read in table mapping locus ids and coordinates (both references)
if(!file.exists(paste("session/",input_file_bg_light,".input_matrix", sep="")))                                     # If matrix exist load from previously saved file if not create, save, and load
{
  geno_matrix_bg_light=read_genotype_probabilities(paste(path_to_data_bg_light, input_file_bg_light,"-geno.txt",sep=""), nindiv_light)
  save(geno_matrix_bg_light, file =paste("session/", input_file_bg_light, ".input_matrix",sep=""))
}
load(paste("session/", input_file_bg_light, ".input_matrix", sep=""))
# ---------------------------------------------------------------------------------------------------------------------#

#ANALYSIS 1
#---------------SNP-specific selection analyses. calculating s, h, delta_p, ...-------------------------------------------------------------------------------------------------------------------#
#---------------DARK--
#---------------Agouti--
if(!file.exists(paste("session/",input_file_dark,".selection_inference_1_locus", sep="")))              # 
{
  results_selection_inference_dark=selection_inference_1_locus(geno_matrix_agouti_dark, lambda_dark, compute_hamming_distances = FALSE, nrep_hamming = 100)
  save(results_selection_inference_dark, file =paste("session/", input_file_dark, ".selection_inference_1_locus",sep=""))
}
load(paste("session/", input_file_dark, ".selection_inference_1_locus", sep=""))
results_sel_inf_1_dark=merge(results_selection_inference_dark$all_snps,map_dark)
results_sel_inf_1_dark_assoc=merge(results_sel_inf_1_dark,annotation)
write.table(results_sel_inf_1_dark, file="results_selection_1_locus_agouti_dark.csv", quote=F, row.names = F)

#---------------Background--
if(!file.exists(paste("session/",input_file_bg_dark,".selection_inference_1_locus", sep="")))              # 
{
  results_selection_inference_bg_dark=selection_inference_1_locus(geno_matrix_bg_dark, lambda_dark, compute_hamming_distances = FALSE, nrep_hamming = 100)
  save(results_selection_inference_bg_dark, file =paste("session/", input_file_bg_dark, ".selection_inference_1_locus",sep=""))
}
load(paste("session/", input_file_bg_dark, ".selection_inference_1_locus", sep=""))
results_sel_inf_1_bg_dark=merge(results_selection_inference_bg_dark$all_snps,map_bg_dark)
results_sel_inf_1_bg_dark_assoc=merge(results_sel_inf_1_bg_dark,annotation)

write.table(results_sel_inf_1_bg_dark, file="results_selection_1_locus_background_dark.csv", quote=F, row.names = F)

#---------------LIGHT--
#---------------Agouti--
if(!file.exists(paste("session/",input_file_light,".selection_inference_1_locus", sep="")))              # 
{
  results_selection_inference_light=selection_inference_1_locus(geno_matrix_agouti_light, lambda_light, compute_hamming_distances = FALSE, nrep_hamming = 100)
  save(results_selection_inference_light, file =paste("session/", input_file_light, ".selection_inference_1_locus",sep=""))
}
load(paste("session/", input_file_light, ".selection_inference_1_locus", sep=""))
results_sel_inf_1_light=merge(results_selection_inference_light$all_snps,map_light)
results_sel_inf_1_light_assoc=merge(results_sel_inf_1_light, annotation)

write.table(results_sel_inf_1_light, file="results_selection_1_locus_agouti_light.csv", quote=F, row.names = F)

#---------------Background--
if(!file.exists(paste("session/",input_file_bg_light,".selection_inference_1_locus", sep="")))              # 
{
  results_selection_inference_bg_light=selection_inference_1_locus(geno_matrix_bg_light, lambda_light, compute_hamming_distances = FALSE, nrep_hamming = 100)
  save(results_selection_inference_bg_light, file =paste("session/", input_file_bg_light, ".selection_inference_1_locus",sep=""))
}
load(paste("session/", input_file_bg_light, ".selection_inference_1_locus", sep=""))
results_sel_inf_1_bg_light=merge(results_selection_inference_bg_light$all_snps,map_bg_light)
results_sel_inf_1_bg_light_assoc=merge(results_sel_inf_1_bg_light,annotation)

write.table(results_sel_inf_1_bg_light, file="results_selection_1_locus_background_light.csv", quote=F, row.names = F)
#--------------------------------------------------------                                                             -------------------------------------------------------------------------------#

#Calculating MAF
#dark soil - Agouti
maf=apply(cbind(results_sel_inf_1_dark[,6],1-results_sel_inf_1_dark[,6]),1,min)
results_sel_inf_1_dark=cbind(results_sel_inf_1_dark, maf)
#dark soil background
maf=apply(cbind(results_sel_inf_1_bg_dark[,6],1-results_sel_inf_1_bg_dark[,6]),1,min)
results_sel_inf_1_bg_dark=cbind(results_sel_inf_1_bg_dark, maf)
#light soil - Agouti
maf=apply(cbind(results_sel_inf_1_light[,6],1-results_sel_inf_1_light[,6]),1,min)
results_sel_inf_1_light=cbind(results_sel_inf_1_light, maf)
#light soil - background
maf=apply(cbind(results_sel_inf_1_bg_light[,6],1-results_sel_inf_1_bg_light[,6]),1,min)
results_sel_inf_1_bg_light=cbind(results_sel_inf_1_bg_light, maf)


#filter on MAF
results_sel_inf_1_dark_10pc=subset(results_sel_inf_1_dark, maf>0.1)
results_sel_inf_1_bg_dark_10pc=subset(results_sel_inf_1_bg_dark, maf>0.1)
results_sel_inf_1_light_10pc=subset(results_sel_inf_1_light, maf>0.1)
results_sel_inf_1_bg_light_10pc=subset(results_sel_inf_1_bg_light, maf>0.1)

#adjusting pvalues for multiple testing (fdr))
#dark soil Agouti
pval_fdr=p.adjust(results_sel_inf_1_dark_10pc$pvalue, method = "fdr")
results_sel_inf_1_dark_10pc_adjusted=cbind(results_sel_inf_1_dark_10pc, pval_fdr )
#dark soil Background
pval_fdr=p.adjust(results_sel_inf_1_bg_dark_10pc$pvalue, method = "fdr")
results_sel_inf_1_bg_dark_10pc_adjusted=cbind(results_sel_inf_1_bg_dark_10pc, pval_fdr )
#light soil agouti
pval_fdr=p.adjust(results_sel_inf_1_light_10pc$pvalue, method = "fdr")
results_sel_inf_1_light_10pc_adjusted=cbind(results_sel_inf_1_light_10pc, pval_fdr )
#light soil background
pval_fdr=p.adjust(results_sel_inf_1_bg_light_10pc$pvalue, method = "fdr")
results_sel_inf_1_bg_light_10pc_adjusted=cbind(results_sel_inf_1_bg_light_10pc, pval_fdr )

#do ks-test
ks_result=ks.test(abs(results_sel_inf_1_dark$delta_p), abs(results_sel_inf_1_light$delta_p), alternative = "greater")


#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  

#ANALYSIS 2
#---------------- NULL MODEL 1 with M-Hypergeo (Probability for each SNP to be a random draw)----------------------------------#
#---------DARK
#---------Agouti
proba_model1_dark=model1(geno_matrix = geno_matrix_agouti_dark, survivors = lambda_dark, min_geno_count, results_sel_inf_1_dark$freq1)             # these probabilities are obtained using the compMWNCHYP with equal weights (converge to MHYP). 
num_sign_obs_dark=sum(proba_model1_dark<=0.01, na.rm=T)                                                                                            # calculate number of significant SNPs at the 1% significance level (Agouti)

#---------Background
proba_model1_bg_dark=model1(geno_matrix = geno_matrix_bg_dark, survivors = lambda_dark, min_geno_count, results_sel_inf_1_bg_dark$freq1)           # these probabilities are obtained using the compMWNCHYP with equal weights (converge to MHYP). 
num_sign_obs_bg_dark=sum(proba_model1_bg_dark<=0.01, na.rm=T)                                                                                      # calculate number of significant SNPs at the 1% significance level (Background)

#---------LIGHT
#---------Agouti
proba_model1_light=model1(geno_matrix = geno_matrix_agouti_light, survivors = lambda_light, min_geno_count, results_sel_inf_1_light$freq1)             # these probabilities are obtained using the compMWNCHYP with equal weights (converge to MHYP). 
num_sign_obs_light=sum(proba_model1_light<=0.01, na.rm=T)                                                                                            # calculate number of significant SNPs at the 1% significance level (Agouti)

#---------Background
proba_model1_bg_light=model1(geno_matrix = geno_matrix_bg_light, survivors = lambda_light, min_geno_count, results_sel_inf_1_bg_light$freq1)           # these probabilities are obtained using the compMWNCHYP with equal weights (converge to MHYP). 
num_sign_obs_bg_light=sum(proba_model1_bg_light<=0.01, na.rm=T)                                                                                      # calculate number of significant SNPs at the 1% significance level (Background)
#-----------------------------------------------------------------------------------------------------------------------------#



#ANALYSIS 3
#----------------NULL MODEL2 (Null distribution of the number of significant SNPs under Null-Model1 MHypergeo)----------------#     
#--------DARK
#--------Agouti
if(!file.exists(paste("session/",input_file_dark,"_",nrep_model2,".model2", sep="")))              # 
{
  dist_sign_model2_dark=model2(geno_matrix_agouti_dark, lambda_dark, nrep_model2, min_geno_count, results_sel_inf_1_dark$freq1)
  save(dist_sign_model2_dark, file =paste("session/", input_file_dark,"_",nrep_model2, ".model2",sep=""))
}
load(paste("session/", input_file_dark,"_",nrep_model2, ".model2", sep=""))

#----------Background
if(!file.exists(paste("session/",input_file_bg_dark,"_",nrep_model2,".model2", sep="")))              # 
{
  dist_sign_model2_bg_dark=model2(geno_matrix_bg_dark, lambda_dark, nrep_model2, min_geno_count, results_sel_inf_1_bg_dark$freq1)
  save(dist_sign_model2_bg_dark, file =paste("session/", input_file_bg_dark,"_",nrep_model2, ".model2",sep=""))
}
load(paste("session/", input_file_bg_dark,"_",nrep_model2, ".model2", sep=""))
#-----------------------------------------------------------------------------------------------------------------------------#     
#--------LIGHT
#--------Agouti
if(!file.exists(paste("session/",input_file_light,"_",nrep_model2,".model2", sep="")))              # 
{
  dist_sign_model2_light=model2(geno_matrix_agouti_light, lambda_light, nrep_model2, min_geno_count, results_sel_inf_1_light$freq1)
  save(dist_sign_model2_light, file =paste("session/", input_file_light,"_",nrep_model2, ".model2",sep=""))
}
load(paste("session/", input_file_light,"_",nrep_model2, ".model2", sep=""))

#----------Background
if(!file.exists(paste("session/",input_file_bg_light,"_",nrep_model2,".model2", sep="")))              # 
{
  dist_sign_model2_bg_light=model2(geno_matrix_bg_light, lambda_light, nrep_model2, min_geno_count, results_sel_inf_1_bg_light$freq1)
  save(dist_sign_model2_bg_light, file =paste("session/", input_file_bg_light,"_",nrep_model2, ".model2",sep=""))
}
load(paste("session/", input_file_bg_light,"_",nrep_model2, ".model2", sep=""))
#-----------------------------------------------------------------------------------------------------------------------------#     

#visualize_null_distribution_model2(dist_sign_model2_bg_dark, dist_sign_model2_bg_light, dist_sign_model2_dark, dist_sign_model2_light, num_sign_obs_bg_dark,num_sign_obs_bg_light, num_sign_obs_dark, num_sign_obs_light)



#ANALYSIS 4 (Model 2 with selection on a single Agouti variant)
#-----------------DARK
#Selection on best candidate 1672 
#-----------------Agouti
if(!file.exists(paste("session/",input_file_dark,"_",nrep_model2_wSelection,".model2_with_selection_on_agouti", sep="")))              # 
{
  dist_sign_model2_wSelection_dark=model2_wSelection_1_locus(geno_matrix_agouti_dark, geno_matrix_for_sel = geno_matrix_agouti_dark, lambda_dark, nrep_model2_wSelection, min_geno_count = 0, results_sel_inf_1_dark,num_sign_obs_dark, results_sel_inf_1_dark$freq1, c(1672))
  save(dist_sign_model2_wSelection_dark, file =paste("session/", input_file_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti",sep=""))
}
load(paste("session/", input_file_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti", sep=""))


#-----------------Background
if(!file.exists(paste("session/",input_file_bg_dark,"_",nrep_model2_wSelection,".model2_with_selection_on_agouti", sep="")))              # 
{
  dist_sign_model2_wSelection_bg_dark = model2_wSelection_1_locus(geno_matrix_bg_dark, geno_matrix_for_sel = geno_matrix_agouti_dark, lambda_dark, nrep_model2_wSelection, min_geno_count = 0, results_sel_inf_1_dark, num_sign_obs_bg_dark, results_sel_inf_1_bg_dark$freq1, c(1672))
  save(dist_sign_model2_wSelection_bg_dark, file = paste("session/", input_file_bg_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti",sep=""))
}
load(paste("session/", input_file_bg_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti", sep=""))
##---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

#Selection on serine deletion 1097
#-----------------Agouti
if(!file.exists(paste("session/",input_file_dark,"_",nrep_model2_wSelection,".model2_with_selection_on_agouti_1097", sep="")))              # 
{
  dist_sign_model2_wSelection_dark_1097=model2_wSelection_1_locus(geno_matrix_agouti_dark, geno_matrix_for_sel = geno_matrix_agouti_dark, lambda_dark, nrep_model2_wSelection, min_geno_count = 0, results_sel_inf_1_dark, num_sign_obs_dark, results_sel_inf_1_dark$freq1, c(1097))
  save(dist_sign_model2_wSelection_dark_1097, file =paste("session/", input_file_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti_1097",sep=""))
}
load(paste("session/", input_file_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti_1097", sep=""))
#-----------------Background
if(!file.exists(paste("session/",input_file_bg_dark,"_",nrep_model2_wSelection,".model2_with_selection_on_agouti_1097", sep="")))              # 
{
  dist_sign_model2_wSelection_bg_dark_1097 = model2_wSelection_1_locus(geno_matrix_bg_dark, geno_matrix_for_sel = geno_matrix_agouti_dark, lambda_dark, nrep_model2_wSelection, min_geno_count = 0, results_sel_inf_1_dark, num_sign_obs_bg_dark, results_sel_inf_1_bg_dark$freq1, c(1097))
  save(dist_sign_model2_wSelection_bg_dark_1097, file = paste("session/", input_file_bg_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti_1097",sep=""))
}
load(paste("session/", input_file_bg_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti_1097", sep=""))
##---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


#-----------------LIGHT
#-----------------Agouti
if(!file.exists(paste("session/",input_file_light,"_",nrep_model2_wSelection,".model2_with_selection_on_agouti", sep="")))              # 
{
  dist_sign_model2_wSelection_light=model2_wSelection_1_locus(geno_matrix_agouti_light, geno_matrix_for_sel = geno_matrix_agouti_light, lambda_light, nrep_model2_wSelection, min_geno_count = 0, results_sel_inf_1_light, num_sign_obs_light, results_sel_inf_1_light$freq1, c(1112))
  save(dist_sign_model2_wSelection_light, file =paste("session/", input_file_light,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti",sep=""))
}
load(paste("session/", input_file_light,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti", sep=""))
#-----------------Background
if(!file.exists(paste("session/",input_file_bg_light,"_",nrep_model2_wSelection,".model2_with_selection_on_agouti", sep="")))              # 
{
  dist_sign_model2_wSelection_bg_light = model2_wSelection_1_locus(geno_matrix_bg_light, geno_matrix_for_sel = geno_matrix_agouti_light, lambda_light, nrep_model2_wSelection, min_geno_count = 0, results_sel_inf_1_light, num_sign_obs_bg_light, results_sel_inf_1_bg_light$freq1, c(1112))
  save(dist_sign_model2_wSelection_bg_light, file = paste("session/", input_file_bg_light,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti",sep=""))
}
load(paste("session/", input_file_bg_light,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti", sep=""))
##---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

                                                                            #VISUALIZATION OF THE COMP. BETWEEN NULL MODEL WITH AND WITHOUT SELECTION
#visualize best agoui candidate
visualize_null_distributions(dist_sign_model2_bg_dark, dist_sign_model2_wSelection_bg_dark, dist_sign_model2_bg_light, dist_sign_model2_wSelection_bg_light, dist_sign_model2_dark, dist_sign_model2_wSelection_dark, dist_sign_model2_light, dist_sign_model2_wSelection_light, nrep_model2, nrep_model2_wSelection, name_of_pdf="model2_selection_or_not_sum_SNPs_sel_on_agouti.pdf")

#visualize serine deletion
visualize_null_distributions(dist_sign_model2_bg_dark, dist_sign_model2_wSelection_bg_dark_1097, dist_sign_model2_bg_light, dist_sign_model2_wSelection_bg_light, dist_sign_model2_dark, dist_sign_model2_wSelection_dark_1097, dist_sign_model2_light, dist_sign_model2_wSelection_light, nrep_model2, nrep_model2_wSelection, name_of_pdf="model2_selection_or_not_sum_SNPs_sel_on_agouti_1097.pdf")



#ANALYSIS 5 
##----------------NULL MODEL 2 with selection (Null distribution of the number of significant SNPs under Null-Model1 with selection (1 locus under selection in Background))-----------------------------#
#-----------------DARK
#-----------------Agouti
if(!file.exists(paste("session/",input_file_dark,"_",nrep_model2_wSelection,".model2_with_selection_on_background", sep="")))              # 
{
  dist_sign_model2_wSelection_dark_sel_on_bg=model2_wSelection_1_locus(geno_matrix_agouti_dark, geno_matrix_for_sel = geno_matrix_bg_dark, lambda_dark, nrep_model2_wSelection, min_geno_count = 0, results_sel_inf_1_bg_dark, compute_hamming_distances = F,num_sign_obs_dark, results_sel_inf_1_dark$freq1, c(39069))
  save(dist_sign_model2_wSelection_dark_sel_on_bg, file =paste("session/", input_file_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_background",sep=""))
}
load(paste("session/", input_file_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_background", sep=""))
#-----------------Background
if(!file.exists(paste("session/",input_file_bg_dark,"_",nrep_model2_wSelection,".model2_with_selection_on_background", sep="")))              # 
{
  dist_sign_model2_wSelection_bg_dark_sel_on_bg = model2_wSelection_1_locus(geno_matrix_bg_dark, geno_matrix_for_sel = geno_matrix_bg_dark, lambda_dark, nrep_model2_wSelection, min_geno_count = 0, results_sel_inf_1_bg_dark, compute_hamming_distances = F,num_sign_obs_bg_dark, results_sel_inf_1_bg_dark$freq1, c(39069))
  save(dist_sign_model2_wSelection_bg_dark_sel_on_bg, file = paste("session/", input_file_bg_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_background",sep=""))
}
load(paste("session/", input_file_bg_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_background", sep=""))
##---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-----------------LIGHT
#-----------------Agouti
if(!file.exists(paste("session/",input_file_light,"_",nrep_model2_wSelection,".model2_with_selection_on_background", sep="")))              # 
{
  dist_sign_model2_wSelection_light_sel_on_bg=model2_wSelection_1_locus(geno_matrix_agouti_light, geno_matrix_for_sel = geno_matrix_bg_light, lambda_light, nrep_model2_wSelection, min_geno_count = 0, results_sel_inf_1_bg_light, compute_hamming_distances = F,num_sign_obs_light, results_sel_inf_1_light$freq1, c(42928))
  save(dist_sign_model2_wSelection_light_sel_on_bg, file =paste("session/", input_file_light,"_",nrep_model2_wSelection, ".model2_with_selection_on_background",sep=""))
}
load(paste("session/", input_file_light,"_",nrep_model2_wSelection, ".model2_with_selection_on_background", sep=""))
#-----------------Background
if(!file.exists(paste("session/",input_file_bg_light,"_",nrep_model2_wSelection,".model2_with_selection_on_background", sep="")))              # 
{
  dist_sign_model2_wSelection_bg_light_sel_on_bg = model2_wSelection_1_locus(geno_matrix_bg_light, geno_matrix_for_sel = geno_matrix_bg_light, lambda_light, nrep_model2_wSelection, min_geno_count = 0, results_sel_inf_1_bg_light, compute_hamming_distances = F,num_sign_obs_bg_light, results_sel_inf_1_bg_light$freq1, c(42928))
  save(dist_sign_model2_wSelection_bg_light_sel_on_bg, file = paste("session/", input_file_bg_light,"_",nrep_model2_wSelection, ".model2_with_selection_on_background",sep=""))
}
load(paste("session/", input_file_bg_light,"_",nrep_model2_wSelection, ".model2_with_selection_on_background", sep=""))
##---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

#VISUALIZATION OF THE COMP. BETWEEN NULL MODEL WITH AND WITHOUT SELECTION
visualize_null_distributions(dist_sign_model2_bg_dark, dist_sign_model2_wSelection_bg_dark_sel_on_bg, dist_sign_model2_bg_light, dist_sign_model2_wSelection_bg_light_sel_on_bg, dist_sign_model2_dark, dist_sign_model2_wSelection_dark_sel_on_bg, dist_sign_model2_light, dist_sign_model2_wSelection_light_sel_on_bg, nrep_model2, nrep_model2_wSelection, name_of_pdf="model2_selection_or_not_sum_SNPs_sel_on_bg.pdf")



#ANALYSIS 6 
#Considering Model2 with selection for all significant variants (dorsal brightness only) in Agouti Dark
#Calculate significance among DB-associated variants only
#dark soil - agouti
annotation_chr4=subset(annotation, chr=="chr4" & pos_chr > 131200000 & pos_chr < 131550000 & phenotype=="Dorsal brightness")                      # process and merge annotation with results
annotated_data_dark=merge(results_sel_inf_1_dark_10pc_adjusted, annotation_chr4, all.x=TRUE)
db_variants_dark=subset(annotated_data_dark, phenotype=="Dorsal brightness")                    #subset variants associated with dorsal brightness
pval_fdr_db=p.adjust(db_variants_dark$pvalue, method="fdr")                                     #adjust pvalues
db_variants_dark=cbind(db_variants_dark, pval_fdr_db)                                           #add adjusted pvalues to results
db_variants_significant_dark=subset(db_variants_dark, pval_fdr_db<=0.05)                        #identify significant DB-associated variants
#-----------------DARK
#-----------------Agouti
if(!file.exists(paste("session/",input_file_dark,"_",nrep_model2_wSelection,".model2_with_selection_on_agouti_all_significant_variants", sep="")))              # 
{
  #dist_sign_model2_wSelection_dark=model2_wSelection_1_locus(geno_matrix_agouti_dark, geno_matrix_for_sel = geno_matrix_agouti_dark, lambda_dark, nrep_model2_wSelection, min_geno_count = 0, results_sel_inf_1_dark, compute_hamming_distances = F,num_sign_obs_dark, results_sel_inf_1_dark$freq1, c(1672))
  dist_sign_model2_wSelection_dark_all_sign_variants=model2_wSelection_1_locus(geno_matrix_agouti_dark, geno_matrix_for_sel = geno_matrix_agouti_dark, lambda_dark, nrep_model2_wSelection, min_geno_count = 0, results_sel_inf_1_dark, num_sign_obs_dark, results_sel_inf_1_dark$freq1, db_variants_significant_dark$id)
  save(dist_sign_model2_wSelection_dark, file =paste("session/", input_file_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti_all_significant_variants",sep=""))
}
load(paste("session/", input_file_dark,"_",nrep_model2_wSelection, ".model2_with_selection_on_agouti_all_significant_variants", sep=""))

#visualize_all_model2_wSelection(dist_sign_model2_wSelection_dark,db_variants_significant_dark$id, num_sign_obs_dark)
