source("include/model1.R")

model2=function(geno_matrix, survivors, nrep, min_geno_count, initial_frequencies)
{
  
  dist_of_number_of_sign_snps=rep(NA,nrep)
  
  for(i in 1:nrep)
  {
    print(i)
    survivors_ran=sample(survivors)
    proba_central=model1(geno_matrix, survivors_ran, min_geno_count, initial_frequencies)
    # calculate numner of significan SNPs at the 1% significance level
    dist_of_number_of_sign_snps[i]=sum(proba_central<=0.01, na.rm=T)                                         
    
  }
  

  return(dist_of_number_of_sign_snps)
  
}